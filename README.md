Newsbox Plugin
================================
Newsbox Plugin for the [**Bludit CMS**](https://www.bludit.com/)

Add content from RSS feed to sidebar. You've to call <your-url>/readFeed to refresh content

License
-------
This is open source software licensed under the [MIT license](https://tldrlegal.com/license/mit-license).
