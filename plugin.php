<?php

class pluginNewsbox extends Plugin {

    public function init()
    {
        $this->dbFields = array(
            'title'=>'',
            'feedurl'=>'',
            'dateFormat'=>'Y-m-d',
            'entries'=>'5',
            'content'=>'',
        );
    }

    public function form()
    {
        global $L;

        $html = '<div>';
        $html .= '<label>'.$L->get('title').'</label>';
        $html .= '<input name="title" class="form-control" type="text" value="'.$this->getValue('title').'">';
        $html .= '</div>';

        $html .= '<div>';
        $html .= '<content>'.$L->get('Feed URL').'</content>';
        $html .= '<input name="feedurl" class="form-control" type="text" value="'.$this->getValue('feedurl').'">';
        $html .= '</div>';

        $html .= '<div>';
        $html .= '<content>'.$L->get('Entries').'</content>';
        $html .= '<input name="entries" class="form-control" type="text" value="'.$this->getValue('entries').'">';
        $html .= '</div>';

        $html .= '<div>';
        $html .= '<content>'.$L->get('Date Format').'</content>';
        $html .= '<input name="dateFormat" class="form-control" type="text" value="'.$this->getValue('dateFormat').'">';
        $html .= '</div>';

        $html .= '<br><br><br><div><b>Attention:</b> Changes will only take effect after open <a href="'.DOMAIN_BASE.'readFeed" target="_blank">'.DOMAIN_BASE.'readFeed</a> . You should open this regularly (e.g. every 5 minutes) by cronjob.</brdiv>';

        return $html;
    }
    public function beforeAll()
    {
        $webhook = 'readFeed';
        if ($this->webhook($webhook)) {
            $this->readFeed();
            exit(0);
        }
    }

    public function readFeed(){
        $feed = $this->getValue('feedurl');
        $entries = $this->getValue('entries');
        $xml = simplexml_load_file($feed);
        if($xml === false) return;
        $html = "";        $lastDate = "";
        for($i = 0; $i < $entries; $i++){
            $title = $xml->channel->item[$i]->title;
            $link = $xml->channel->item[$i]->link;
            $date = date($this->getValue('dateFormat'),strtotime($xml->channel->item[$i]->pubDate));
            if($date !== $lastDate){
                if($lastDate !== ""){
                    $html .= '</ul>';
                }
                $html .= '<small>'.$date.'</small><ul>';
            }
            $html .= '<li><a href="'.$link.'">'.$title.'</a></li>';
            $lastDate = $date;
        }
        $this->setField('content', $html);
    }

    public function siteSidebar()
    {
        if( $this->getValue('content') == '' ) {
            return '';
        }
        $html  = '<div class="plugin plugin-pages">';
        if ($this->getValue('title')) {
            $html .= '<h2 class="plugin-label">'.$this->getValue('title',false).'</h2>';
        }
        $html .= '<div class="plugin-content">';
        $html .= html_entity_decode($this->getValue('content'));
        $html .= '</div>';
        $html .= '</div>';

        return $html;
    }
}